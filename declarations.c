#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 16000000UL
#define delai_leds 50000 // prescale de 64 → 5Hz ou 200ms

typedef unsigned char uchar;
uchar broche[10] = {2,3,1,0,4,6,7,6,4,5}; //n of PORTXn
uchar port[10] = {2,2,2,2,2,1,2,3,0,0}; // B=0, then +3

// ignite or not (bool e) the i-esth LED
void affiche(uchar i, char e){
	(e) ? (*(uchar*)(0x25 + 3*port[i]) |= 1<<broche[i]) :
	      (*(uchar*)(0x25 + 3*port[i]) &= ~(1<<broche[i]));
}

void init(void) {
	// Déclarations des registres
	for (uchar i = 0; i < 10; ++i) { // sorties et mise à 0
		*(uchar*)(0x24 + 3*port[i]) |= 1<<broche[i];
		*(uchar*)(0x25 + 3*port[i]) &= ~(1<<broche[i]);
	}

	// Bouton
	DDRD &= ~(1<<PORTD2); // D0 relié à D11
	PORTD |= 1<<PORTD2; // Bouton en Pull-Up
	PIND &= ~(1<<PORTD2); // Init du bouton à 0

	// Interruption
	MCUCR &=~(1<<PUD);
//	EICRA = 0x30; // rising edge
	EIMSK = 1<<INT2; // enable INT2
	USBCON = 0; // desactive l’interruption sur USB
	sei(); // enable interrupts
}

/* timer 16 bits: overflow avec Δt = prescale / F_CPU → T = 0xFFFF * Δt
 *                CTC avec même Δt → T = OCRnA * Δt
 *                      → OCRnA = F_CPU / (Freq_voulue * prescale)
 */

void timer_init(void){  // Timer1 avec prescaler=64 et CTC (WGM=2)
	TCCR1B |= (1 << WGM12)|(1 << CS11)|(1 << CS10);
	OCR1A = delai_leds; // comparaison CTC
}
