#include <avr/io.h>
#include <avr/interrupt.h>
#include "declarations.h" // affiche & init & timer
char sens = 1;

ISR(INT2_vect){
	sens ^= 1; // flag changement de sens
	TIFR1 |= (1 << OCF1A); // clear flag
}

int main(void){
	init();
	timer_init();
	unsigned char x = 1;

	while(1){
		if (TIFR1 & (1 << OCF1A)){
			TIFR1 |= (1 << OCF1A); // clear flag
			if (sens) { // sens positif
				if (x > 9) {x = 1;}
				(x != 1) ? affiche(x-1, 0) : affiche(9, 0);
				affiche(x, 1); 
				x++;
			} else { // sens inverse
				if (x < 1) {x = 9;}
				(x != 9) ? affiche(x+1, 0): affiche(1, 0);
				affiche(x, 1);
				x--;
			}
		}
	}
	return 0;
}
